<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@utama');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/master', function() {
    return view('adminlte.master');
}); 

Route::get('/page/table', function(){
    return view('page.table');
});

Route::get('/page/data-table', function(){
    return view('page.data-table');
});

Route::get('/page', function(){
    return view('page.index');
});