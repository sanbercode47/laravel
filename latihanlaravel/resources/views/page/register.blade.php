<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Sign Up Page</title>
  </head>
  <body>
    <h1>Buat Akun Baru !</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
    @csrf
      <label for="first_name">First Name : </label><br />
      <input type="text" name="first_name" id="first_name" /><br />
      <label for="last_name">Last Name : </label><br />
      <input type="text" name="last_name" id="last_name" /><br />
      <label for="gender">Gender : </label><br />
      <input type="radio" name="gender" id="gender" /> Male <br />
      <input type="radio" name="gender" id="gender" /> Female <br />
      <input type="radio" name="gender" id="gender" /> Other <br />
      <label for="nationality">Nationality : </label><br />
      <select name="nationality" id="nationality">
        <option value="Indonesia">Indonesia</option>
        <option value="Singapura">Singapura</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Australia">Australia</option></select
      ><br />
      <label for="language">Language Spokes : </label> <br />
      <input type="checkbox" name="language" id="language" /> Bahasa Indonesia <br />
      <input type="checkbox" name="language" id="language" /> English <br />
      <input type="checkbox" name="language" id="language" /> Other <br />
      <label for="bio">Bio : </label><br />
      <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>